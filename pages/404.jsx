import { useEffect } from 'react'
import { useRouter } from 'next/router'

import Heading from '../components/chunks/Heading/Heading'
import Seo from '../components/chunks/Seo/Seo'

import styles from '../styles/404.module.scss'

const Error = () => {
  const router = useRouter()

  useEffect(() => {
    setTimeout(() => {
      router.push('/')
    }, 3000)
  }, [router])

  return (
    <>
      <Seo title="error page" />
      <div className={styles.wrapper}>
        <div>
          <Heading text="404" />
          <Heading text="no page found" tag="h2" />
        </div>
      </div>
    </>
  )
}

export default Error
