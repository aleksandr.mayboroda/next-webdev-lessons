import Link from 'next/link'

import Seo from '../../components/chunks/Seo/Seo'
import Heading from '../../components/chunks/Heading/Heading'
import A from '../../components/chunks/A/A'

const Contacts = ({ contacts }) => {
  const list =
    !contacts?.length > 0
      ? null
      : contacts.map(({ id, name, email }) => (
          <li key={id}>
            <A href={`/contacts/${id}`}>
              <strong>{name}</strong> {email}
            </A>
          </li>
        ))

  return (
    <>
      <Seo title="contacts list page" />
      <div>
        <Heading text="Contacts list page" />
      </div>
      <ul>{list}</ul>
    </>
  )
}

export async function getStaticProps() {
  const contactsResult = await fetch(
    `https://jsonplaceholder.typicode.com/users`
  )
  const contacts = await contactsResult.json()
  // const contacts = null

  if (!contacts) {
    return { notFound: true }
  }
  return {
    props: { contacts }, // will be passed to the page component as props
  }
}

export default Contacts
