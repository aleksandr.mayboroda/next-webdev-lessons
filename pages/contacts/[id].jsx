import Seo from '../../components/chunks/Seo/Seo'
import ContactInfo from '../../components/blocks/ContactInfo/ContactInfo'

const Contact = ({ contact }) => {
  console.log(contact)
  return (
    <>
      <Seo title={`contact page for ${contact.name}`} />
      <div>
        <ContactInfo contact={contact} />
      </div>
    </>
  )
}

export async function getServerSideProps({ params: { id } }) {
  // console.log(id)
  const contactsResult = await fetch(
    `https://jsonplaceholder.typicode.com/users/${id}`
  )
  const contact = await contactsResult.json()
  // const contact = null

  if (!contact) {
    return { notFound: true }
  }
  return {
    props: { contact }, // will be passed to the page component as props
  }
}

export default Contact
