import Seo from '../../components/chunks/Seo/Seo'
import PostInfo from '../../components/blocks/PostInfo/PostInfo'

const Post = ({ post }) => {
  return (
    <>
      <Seo title={`post ${post?.title}`} />
      <PostInfo post={post} />
    </>
  )
}

export const getStaticPaths = async () => {
  const postsResult = await fetch(`https://jsonplaceholder.typicode.com/posts`)
  const posts = await postsResult.json()

  const paths = posts.map(({ id }) => ({ params: { id: id.toString() } })) //перевод в строку обязательно!!!

  return {
    paths,
    fallback: false, // if error === returns 404
  }
}

export async function getStaticProps({ params: { id } }) {
  const postResult = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  )
  const post = await postResult.json()
  // const post = null

  if (!post) {
    return { notFound: true }
  }

  return {
    props: { post },
  }
}

export default Post
