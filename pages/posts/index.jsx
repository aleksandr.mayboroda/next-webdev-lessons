import Seo from '../../components/chunks/Seo/Seo'
import Heading from '../../components/chunks/Heading/Heading'
import A from '../../components/chunks/A/A'

const Posts = ({ posts }) => {
  const list =
    !posts?.length > 0
      ? null
      : posts.map(({ id, title }) => (
          <li key={id}>
            <A text={title} href={`/posts/${id}`} />
          </li>
        ))
  return (
    <>
      <Seo title="Posts" />
      <div>
        <Heading text="Posts list:" />
        <ul>{list}</ul>
      </div>
    </>
  )
}

export async function getStaticProps() {
  const contactsResult = await fetch(
    `https://jsonplaceholder.typicode.com/posts`
  )
  const posts = await contactsResult.json()
  // const posts = null

  if (!posts) {
    return { notFound: true }
  }
  
  return {
    props: { posts }, // will be passed to the page component as props
  }
}

export default Posts
