// import Head from 'next/head'
// import Image from 'next/image'
import styles from '../styles/Home.module.scss'

import Seo from '../components/chunks/Seo/Seo'
import Heading from '../components/chunks/Heading/Heading'
import Socials from '../components/blocks/Socials/Socials'

const Home = ({ socials }) => {
  return (
    <>
      <Seo title="home page" />
      <div className={styles.wrapper}>
        <Heading text="Hello World!" />
        <Socials socials={socials} />
      </div>
    </>
  )
}

export async function getStaticProps(context) {
  try {
    const socialsResult = await fetch(`${process.env.API_HOST}/socials`)
    const socials = await socialsResult.json()

    if (!socials) {
      return { notFound: true }
    }

    return {
      props: { socials }, // will be passed to the page component as props
    }
  } catch (error) {
    return {
      props: { socials: null }, // will be passed to the page component as props
    }
  }
}

export default Home
