// import Image from 'next/image'
import Head from 'next/head'

import '../styles/globals.scss'

import MainLayout from '../components/layouts/MainLayout'

import testImage from '../public/test.jpg'

const MyApp = ({ Component, pageProps }) => {
  return (
    <MainLayout>
      {/* скрипты добавляются иначе, пуием доавления их в отдельный файл _document.js: https://nextjs.org/docs/basic-features/font-optimization */}
      {/* <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap"
          rel="stylesheet"
        />
      </Head> */}
      <main>
        <Component {...pageProps} />
      </main>
      {/* <Image
        src={testImage}
        width={600}
        height={350}
        alt={'preview'}
        placeholder={'blur'}
      /> */}
    </MainLayout>
  )
}

export default MyApp
