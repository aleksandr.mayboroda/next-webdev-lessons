import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'

import styles from '../../../styles/Navbar.module.scss'

const navigation = [
  { id: 1, title: 'Home', path: '/' },
  { id: 2, title: 'Posts', path: '/posts' },
  { id: 3, title: 'Contacts', path: '/contacts' },
]

const Navbar = () => {
  const { pathname } = useRouter()
  const linkList = navigation.map(({ id, title, path }) => (
    <Link href={path} key={id}>
      <a className={pathname === path ? styles.active : null}>{title}</a>
    </Link>
  ))
  return (
    <nav className={styles.nav}>
      <div className={styles.logo}>
        <Image src="/logo.png" alt="logo" width={60} height={60} />
      </div>
      <div className={styles.links}>{linkList}</div>
    </nav>
  )
}

export default Navbar
