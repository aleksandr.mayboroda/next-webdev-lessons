import React from 'react'

import Heading from '../../chunks/Heading/Heading'

const Footer = () => {
  return (
    <footer>
      <Heading text="Footer" />
    </footer>
  )
}

export default Footer
