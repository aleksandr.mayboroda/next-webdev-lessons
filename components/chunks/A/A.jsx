import Link from 'next/link'

const A = ({ text = '', href, className = null, children = null }) => {
  return (
    <Link href={href}>
      <a className={className}>{text ? text : children}</a>
    </Link>
  )
}

export default A
