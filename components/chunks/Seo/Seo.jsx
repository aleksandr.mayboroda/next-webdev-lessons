import Head from 'next/head'

const Seo = ({title="default page title", description=''}) => {
  return (
    <Head>
      <title>{title}</title>

    </Head>
  )
}

export default Seo