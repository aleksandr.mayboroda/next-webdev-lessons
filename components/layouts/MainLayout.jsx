import Header from '../business/Header/Header'
import Footer from '../business/Footer/Footer'

const MainLayout = ({ children }) => {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  )
}

export default MainLayout
