import Heading from '../../chunks/Heading/Heading'

const ContactInfo = ({ contact }) => {
  const { name, email, address } = contact || {}
  const { street, suite, city, zipcode } = address || {}

  if (!contact) {
    return <Heading text={'Contact is empty'} tag="h3" />
  }

  return (
    <div>
      <Heading tag="h3" text={name} />
      <div>
        <strong>Email: </strong>
        {email}
      </div>
      <div>
        <strong>Address: </strong>
        {`${street}, ${suite}, ${city}, ${zipcode}`}
      </div>
    </div>
  )
}

export default ContactInfo
